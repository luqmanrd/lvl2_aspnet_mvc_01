﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class CodingIDController : Controller
    {
        // GET: CodingID
        [Route("CodingID")]
        [HttpGet]
        public ActionResult index()
        {
            string html = "<form method='post'>" +
                "<input type='text' name='name'/>" +
                "<input type='submit' name='Greet Me'/>" +
                "</form>";
            return Content(html, "text/html");
        }
        [Route("CodingID")]
        [HttpPost]
        public ActionResult display(string name = "World")
        {
            return Content(string.Format("<h1>Hello "+name+"</h1>"), "text/html");
        }
        public ActionResult CodingIDMessage()
        {
            return View();
        }

        [Route("CodingID/Aloha")]
        public ActionResult Goodbye()
        {
            return Content("Goodbye");
        }
    }
}